package pl.edu.uwm.wmii.solisdariusz.laboratorium07;

public class Adres{

    private String ulica;
    private int numer_domu;
    private int numer_mieszkania;   //opcjonalne
    private String miasto;
    public String kod_pocztowy;

    public Adres(String ulica, int numer_domu, String miasto, String kod_pocztowy){
        this.ulica=ulica;
        this.numer_domu=numer_domu;
        this.miasto=miasto;
        this.kod_pocztowy=kod_pocztowy;
    }

    public Adres(String ulica, int numer_domu, int numer_mieszkania, String miasto, String kod_pocztowy){
        this.ulica=ulica;
        this.numer_domu=numer_domu;
        this.numer_mieszkania=numer_mieszkania;
        this.miasto=miasto;
        this.kod_pocztowy=kod_pocztowy;
    }

    public void show(){
        System.out.println(kod_pocztowy+" "+miasto);
        if(numer_mieszkania==0)
            System.out.println(ulica+", "+numer_domu);
        else
            System.out.println(ulica+", "+numer_domu+", "+numer_mieszkania);
    }

    public Boolean publicboolean(String kod_pocztowy2){
        String new1=kod_pocztowy.replaceAll("-", "");
        String new2=kod_pocztowy2.replaceAll("-", "");
        int kod1_int=Integer.parseInt(new1);
        int kod2_int=Integer.parseInt(new2);
        if(kod1_int>kod2_int) return true;
        else return false;
    }
}