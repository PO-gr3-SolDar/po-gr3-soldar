package pl.edu.uwm.wmii.solisdariusz.laboratorium07;
import pl.imiajd.Solis.*;
public class Main {

    public static void main(String[] args) {
        Osoba ja = new Osoba("Solis","Dariusz");
        Student ktos = new Student("Ktosiowy","Ktos","kulturoznawstwo");
        Nauczyciel bozena = new Nauczyciel("Szczesliwa","Bożena",10000);
        System.out.println(ja.toString());
        System.out.println(ktos.toString());
        System.out.println(bozena.toString());
        System.out.print(ja.getNazwisko()+" "+ja.getImie()+"\n");
        System.out.print(ktos.getNazwisko()+" "+ktos.getImie()+" "+ktos.getKierunek()+"\n");
        System.out.print(bozena.getNazwisko()+" "+bozena.getImie()+" "+bozena.getPensja()+"\n");
    }
}
