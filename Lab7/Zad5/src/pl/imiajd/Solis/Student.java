package pl.imiajd.Solis;

public class Student extends Osoba{

    private String nazwisko;
    private String imie;
    private String kierunek;

    public Student(String nazwisko, String imie, String kierunek){
        this.nazwisko=nazwisko;
        this.imie=imie;
        this.kierunek=kierunek;
    }

    public String toString(){
        String wynik="";
        wynik=imie+" "+nazwisko+" "+kierunek;
        return wynik;
    }

    public String getImie(){
        return this.imie;
    }

    public String getNazwisko(){
        return this.nazwisko;
    }

    public String getKierunek(){
        return kierunek;
    }

}