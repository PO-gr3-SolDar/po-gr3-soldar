package pl.edu.uwm.wmii.solisdariusz.laboratorium07;

public class Osoba{

    private String nazwisko;
    private String imie;

    public Osoba(String nazwisko, String imie){
        this.nazwisko=nazwisko;
        this.imie=imie;
    }

    public Osoba(){
    }

    public String toString(){
        String wynik="";
        wynik=imie+" "+nazwisko;
        return wynik;
    }

    public String getImie(){
        return this.imie;
    }

    public String getNazwisko(){
        return this.nazwisko;
    }

}