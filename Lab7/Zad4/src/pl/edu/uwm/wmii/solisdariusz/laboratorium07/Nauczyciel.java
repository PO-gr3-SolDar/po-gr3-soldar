package pl.edu.uwm.wmii.solisdariusz.laboratorium07;

public class Nauczyciel extends Osoba{

    private double pensja;
    private String nazwisko;
    private String imie;

    public Nauczyciel(String nazwisko, String imie, double pensja){
        this.nazwisko=nazwisko;
        this.imie=imie;
        this.pensja=pensja;
    }

    public String toString(){
        String wynik="";
        wynik=imie+" "+nazwisko+" "+pensja;
        return wynik;
    }

    public String getImie(){
        return this.imie;
    }

    public String getNazwisko(){
        return this.nazwisko;
    }

    public double getPensja(){
        return pensja;
    }

}
