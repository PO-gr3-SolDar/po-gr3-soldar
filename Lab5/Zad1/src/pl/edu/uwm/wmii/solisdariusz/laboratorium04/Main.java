package pl.edu.uwm.wmii.solisdariusz.laboratorium04;

import java.util.ArrayList;

public class Main {
    public static ArrayList<Integer> append(ArrayList<Integer> a, ArrayList<Integer> b){
        for(int element : b){
            a.add(element);
        }
        return a;
    }

    public static void main(String[] args) {
        ArrayList<Integer> a=new ArrayList<Integer>();
        a.add(1);a.add(4);a.add(9);a.add(16);
        ArrayList<Integer> b=new ArrayList<Integer>();
        b.add(9);b.add(7);b.add(4);b.add(9);b.add(11);
        System.out.println("lista a: "+a);
        System.out.println("lista b: "+b);
        System.out.println(append(a,b));
    }
}