package pl.edu.uwm.wmii.solisdariusz.laboratorium05;

import java.util.ArrayList;

public class Zad3 {
    public static ArrayList<Integer> mergeSorted(ArrayList<Integer> a, ArrayList<Integer> b){
        ArrayList<Integer> c=new ArrayList<Integer>();
        if(a.size()<b.size()){
            for(int i=0; i<b.size(); i++){
                if(i>=a.size())
                    c.add(b.get(i));
                else{
                    c.add(a.get(i));
                    c.add(b.get(i));
                }
                for(int j=0; j<c.size();j++){

                }
            }
        }
        else{
            for(int i=0; i<b.size(); i++) {
                if (i >= b.size())
                    c.add(a.get(i));
                else {
                    c.add(a.get(i));
                    c.add(b.get(i));
                }
            }
        }
        int pom=0;
        for(int i=0; i<c.size(); i++){
            for(int j=i+1; j<c.size(); j++){
                if(c.get(j)<c.get(i)){
                    pom=c.get(i);
                    c.set(i, c.get(j));
                    c.set(j, pom);
                }
            }
        }
        return c;
    }

    public static void main(String[] args) {
        ArrayList<Integer> a=new ArrayList<Integer>();
        a.add(1);a.add(4);a.add(9);a.add(16);
        ArrayList<Integer> b=new ArrayList<Integer>();
        b.add(9);b.add(7);b.add(4);b.add(9);b.add(11);b.add(12);b.add(14);b.add(10);
        System.out.println("lista a: "+a);
        System.out.println("lista b: "+b);
        System.out.println(mergeSorted(a,b));
    }
}