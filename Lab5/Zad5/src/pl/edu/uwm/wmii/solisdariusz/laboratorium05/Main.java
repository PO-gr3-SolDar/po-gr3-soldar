package pl.edu.uwm.wmii.solisdariusz.laboratorium05;

import java.util.ArrayList;

public class Main {
    public static ArrayList<Integer> a=new ArrayList<Integer>();

    public static void reverse(ArrayList<Integer> a){
        int pom=0;
        int pom2=0;
        for(int i=0; i<a.size()/2; i++){
            pom=a.get(i);
            pom2=a.get(a.size()-i-1);
            a.set(i, pom2);
            a.set(a.size()-i-1, pom);
        }
    }

    public static void main(String[] args) {
        a.add(1);a.add(4);a.add(9);a.add(16);
        System.out.println("lista a: "+a);
        reverse(a);
        System.out.println("lista a po użyciu reverse(): "+a);
    }
}