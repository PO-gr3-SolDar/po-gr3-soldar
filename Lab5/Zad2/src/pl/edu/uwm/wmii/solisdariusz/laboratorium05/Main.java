package pl.edu.uwm.wmii.solisdariusz.laboratorium05;

import java.util.ArrayList;

public class Main {
    public static ArrayList<Integer> merge(ArrayList<Integer> a, ArrayList<Integer> b){
        ArrayList<Integer> c=new ArrayList<Integer>();
        if(a.size()<b.size()){
            for(int i=0; i<b.size(); i++){
                if(i>=a.size())
                    c.add(b.get(i));
                else{
                    c.add(a.get(i));
                    c.add(b.get(i));
                }
            }
        }
        else{
            for(int i=0; i<b.size(); i++) {
                if (i >= b.size())
                    c.add(a.get(i));
                else {
                    c.add(a.get(i));
                    c.add(b.get(i));
                }
            }
        }
        return c;
    }

    public static void main(String[] args) {
        ArrayList<Integer> a=new ArrayList<Integer>();
        a.add(1);a.add(4);a.add(9);a.add(16);
        ArrayList<Integer> b=new ArrayList<Integer>();
        b.add(9);b.add(7);b.add(4);b.add(9);b.add(11);b.add(12);b.add(14);b.add(10);
        System.out.println("lista a: "+a);
        System.out.println("lista b: "+b);
        System.out.println(merge(a,b));
    }
}