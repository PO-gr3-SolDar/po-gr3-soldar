package pl.edu.uwm.wmii.solisdariusz.laboratorium05;

import java.util.ArrayList;

public class Main {

    public static ArrayList<Integer> reverse(ArrayList<Integer> a){
        int pom=0;
        int pom2=0;
        for(int i=0; i<a.size()/2; i++){
            pom=a.get(i);
            pom2=a.get(a.size()-i-1);
            a.set(i, pom2);
            a.set(a.size()-i-1, pom);
        }
        return a;
    }

    public static void main(String[] args) {
        ArrayList<Integer> a=new ArrayList<Integer>();
        a.add(1);a.add(4);a.add(9);a.add(16);
        System.out.println("lista a: "+a);
        System.out.println(reverse(a));
    }
}