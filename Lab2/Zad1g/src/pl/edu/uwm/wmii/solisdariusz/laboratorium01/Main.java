package pl.edu.uwm.wmii.solisdariusz.laboratorium01;

import java.util.Scanner;

public class Main {

    public static void main(String[] args)
    {
        Scanner myInput = new Scanner(System.in);
        System.out.println("Podaj liczbe n");
        int n = myInput.nextInt();
        double wpisana;
        double wynik1 = 0;
        double wynik2 = 1;
        int i;
        double[] tab = new double[n];
        for(i = 0 ; i < n ; i++)
        {
            System.out.println("Wpisz " + i + " wyraz ciagu");
            wpisana = myInput.nextDouble();

                tab[i] = wpisana;
                wynik1 += tab[i];
                wynik2 *= tab[i];
        }
        System.out.println("Wynik dodawania a1->an to: " + wynik1);
        System.out.println("Wynik mnożenia a1->an to: " + wynik2);
    }
}
