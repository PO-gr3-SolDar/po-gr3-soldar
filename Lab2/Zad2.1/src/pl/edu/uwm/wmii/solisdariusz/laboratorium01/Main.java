package pl.edu.uwm.wmii.solisdariusz.laboratorium01;
import java.util.Scanner;
//Zad 2 pt 1
public class Main {

    public static void main(String[] args) {
        Scanner myInput = new Scanner(System.in);
        System.out.println("Podaj liczbe n");
        int n = myInput.nextInt();
        int i;
        int wczytana;
        int[] tab = new int[n];
        int[] tab_pomoc_h = new int[n];
        int wynik_a = 0;
        int wynik_b = 0;
        int wynik_c = 0;
        int wynik_d = 0;
        int wynik_e = 0;
        int wynik_f = 0;
        int wynik_g = 0;
        int wynik_h = 0;
        int pomoc_silnia = 1;
        int[] parzyste = new int[5001];
        int z = 0;
        for(i = 0 ; i < 10000; i++)
        {
            if(i%2 == 0)
            {
                parzyste[z] = i;
                z++;
            }
        }
        for(i = 0 ; i < n ; i++)
        {
            wczytana = myInput.nextInt();
            tab[i] = wczytana;
        }
        //a)
        for(i = 0 ; i < n ; i++)
        {
            if(tab[i]%2 != 0)
                wynik_a += 1;
        }
        //b)
        for(i = 0 ; i < n ; i++)
        {
            if(tab[i]%3 == 0 && tab[i]%5 != 0)
                wynik_b += 1;
        }
        //c)
        for(i = 0 ; i < n ; i++)
        {
            for(int y = 0 ; y < 5001 ; y++)
            {
                if(tab[i] == parzyste[y])
                {
                    wynik_c += 1;
                }
            }
        }
        //d)
        for(int k = 1 ; k < n-1 ; k++)
        {
            if(tab[k] > (tab[k-1]+tab[k+1])/2)
            {
                wynik_d += 1;
            }
        }
        //e
        for(i = 1 ; i < n+1 ; i++)
        {
            pomoc_silnia *= i;
            if(tab[i-1] > Math.pow(2,i) && tab[i-1] < pomoc_silnia && i <= n)
            {
                wynik_e += 1;
            }
        }
        //f
        for(i = 0 ; i < n ; i++)
        {
            if(i%2 != 0 && tab[i]%2 == 0)
            {
                wynik_f += 1;
            }
        }
        //g
        for(i = 0 ; i < n ; i++)
        {
            if(tab[i]%2 != 0 && tab[i] >= 0)
            {
                wynik_g += 1;
            }
        }
        //h
        for(i = 0 ; i < n ; i++)
        {
            tab_pomoc_h[i] = tab[i];
            if(tab[i] < 0)
            {
                tab_pomoc_h[i] = -tab_pomoc_h[i];
            }
            if(tab_pomoc_h[i] < Math.pow(i,2))
            {
                wynik_h++;
            }
        }
        System.out.println("Wyniki od zad a do zad h: ");
        System.out.println(wynik_a);
        System.out.println(wynik_b);
        System.out.println(wynik_c);
        System.out.println(wynik_d);
        System.out.println(wynik_e);
        System.out.println(wynik_f);
        System.out.println(wynik_g);
        System.out.println(wynik_h);

    }
}

