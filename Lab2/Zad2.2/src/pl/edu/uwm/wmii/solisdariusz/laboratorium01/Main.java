package pl.edu.uwm.wmii.solisdariusz.laboratorium01;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner myInput = new Scanner(System.in);
        System.out.println("Podaj liczbe n");
        int n = myInput.nextInt();
        Double[] tab = new Double[n];
        double wczytana;
        double suma = 0;
        for(int i = 0 ; i < n ; i++)
        {
            wczytana = myInput.nextDouble();
            tab[i] = wczytana;
        }
        for(int y = 0 ; y < n ; y++)
        {
            if(tab[y] > 0)
            {
                suma += tab[y]*2;
            }
        }
        System.out.println(suma);
    }
}
