package pl.edu.uwm.wmii.solisdariusz.laboratorium01;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner myInput = new Scanner(System.in);
        System.out.println("Podaj liczbe n");
        int n = myInput.nextInt();
        Double[] tab = new Double[n];
        double pomoc_min;
        double pomoc_max;
        double wczytana;
        for(int i = 0 ; i < n ; i++)
        {
            wczytana = myInput.nextDouble();
            tab[i] = wczytana;
        }
        pomoc_min = tab[0];
        pomoc_max = tab[0];
        for(int y = 0 ; y < n ; y++)
        {
            if(tab[y] > pomoc_max)
                pomoc_max = tab[y];
            if(tab[y] < pomoc_min)
                pomoc_min = tab[y];
        }
        System.out.println("min "+pomoc_min);
        System.out.println("max "+pomoc_max);
    }
}
