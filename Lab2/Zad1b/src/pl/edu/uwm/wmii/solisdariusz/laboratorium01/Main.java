package pl.edu.uwm.wmii.solisdariusz.laboratorium01;
import java.util.Scanner;
public class Main {

    public static void main(String[] args)
    {
        Scanner myInput = new Scanner(System.in);
        System.out.println("Podaj liczbe n");
        int n = myInput.nextInt();
        float wpisana;
        float wynik = 1;
        int i;
        float[] tab = new float[n];
        for(i = 0 ; i < n ; i++)
        {
            System.out.println("Wpisz " + i + " wyraz ciagu");
            wpisana = myInput.nextInt();
            tab[i] = wpisana;
            wynik *= tab[i];
        }
        System.out.println("Wynik to: " + wynik);
    }
}
