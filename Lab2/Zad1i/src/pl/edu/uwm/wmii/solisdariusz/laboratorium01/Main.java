package pl.edu.uwm.wmii.solisdariusz.laboratorium01;

import java.util.Scanner;

public class Main {

    public static void main(String[] args)
    {
            Scanner myInput = new Scanner(System.in);
            System.out.println("Podaj liczbe n");
            int silnia = 1;
            int n = myInput.nextInt();
            double wynik = 0;
            double[] tab = new double[n];

            for(int i = 1; i != n+1; i++)
            {
                double wpisana = myInput.nextDouble();
                tab[i-1] = (((Math.pow(-1,i))*wpisana)/silnia);
                silnia *= i;
                wynik += tab[i-1];
            }

            System.out.println("Wynik to: " + wynik);
    }
}
