package pl.edu.uwm.wmii.solisdariusz.laboratorium01;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner myInput = new Scanner(System.in);
        System.out.println("Podaj liczbe n");
        int n = myInput.nextInt();
        double wpisana;
        double wynik = 0;
        int i;
        double[] tab = new double[n];
        for(i = 0 ; i < n ; i++)
        {
            System.out.println("Wpisz " + i + " wyraz ciagu");
            wpisana = myInput.nextDouble();
            tab[i] = Math.pow(wpisana,2);
            wynik += tab[i];
        }
        System.out.println("Wynik to: " + wynik);    }
}
