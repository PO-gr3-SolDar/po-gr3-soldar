package pl.edu.uwm.wmii.solisdariusz.laboratorium01;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner myInput = new Scanner(System.in);
        System.out.println("Podaj liczbe n");
        int n = myInput.nextInt();
        if(n < 4)
        {
            System.out.println("Za krotki ciag");
            return;
        }
        int i;
        double[] tab = new double[n];
        double[] tab_pomoc = new double[n];

        for (i = 1 ; i != n + 1 ; i++) {
            double wpisana = myInput.nextDouble();
            tab[i - 1] = wpisana;
        }
        tab_pomoc[0] = tab[1];
        tab_pomoc[n-1] = tab[0];
        for(i = 1 ; i != n-1 ; i++)
        {
            tab_pomoc[i] = tab[i+1];
        }
        for(i = 0 ; i < n ; i++)
        {
            System.out.println(tab_pomoc[i]);
        }

    }
}
