package pl.edu.uwm.wmii.solisdariusz.laboratorium01;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner myInput = new Scanner(System.in);
        System.out.println("Podaj liczbe n");
        int n = myInput.nextInt();
        Double[] tab = new Double[n];
        double wczytana;
        int suma_uj = 0;
        int suma_zer = 0;
        int suma_dod = 0;
        for(int i = 0 ; i < n ; i++)
        {
            wczytana = myInput.nextDouble();
            tab[i] = wczytana;
        }
        for(int y = 0 ; y < n ; y++)
        {
            if(tab[y] > 0){ suma_dod += 1;}
            else if(tab[y] == 0){suma_zer += 1;}
            else{suma_uj += 1;}
        }
        System.out.println("ujemne"+suma_uj);
        System.out.println("zera"+suma_zer);
        System.out.println("dodatnie"+suma_dod);
    }
}
