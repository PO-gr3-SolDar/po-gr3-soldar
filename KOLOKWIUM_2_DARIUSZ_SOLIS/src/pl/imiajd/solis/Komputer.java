package pl.imiajd.solis;
import java.time.LocalDate;
import java.util.LinkedList;

public class Komputer implements Cloneable, Comparable<Komputer>{
    public Komputer(String nazwa, LocalDate dataProdukcji){
        this.nazwa=nazwa;
        this.dataProdukcji=dataProdukcji;
    }
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Komputer komputer = (Komputer) o;
        return nazwa.equals(komputer.nazwa) &&
                dataProdukcji.equals(komputer.dataProdukcji);
    }

    public int compareTo(Komputer T) {
        if(this.nazwa==T.nazwa)
            if(this.dataProdukcji==T.dataProdukcji)
                return 1;
        return 0;
    }

    public Komputer clone() throws CloneNotSupportedException {
        Komputer cloned = (Komputer) super.clone();
        cloned.nazwa = nazwa;
        cloned.dataProdukcji = dataProdukcji;
        return cloned;
    }
    //Zadanie2
    public static void redukuj(LinkedList<String> komputery, int n){
        int licznik=1;
        for(int i=0; i< komputery.size(); i++)
        {
            if(licznik==n)
            {
                komputery.remove(licznik);
                licznik=1;
            }
            else
            {
                licznik++;
            }
        }
    }
    //Koniec Zadania2
    private String nazwa;
    private LocalDate dataProdukcji;
}
