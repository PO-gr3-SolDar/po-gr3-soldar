package pl.imiajd.solis;
import pl.imiajd.solis.Komputer;

import java.time.LocalDate;

public class Laptop extends Komputer implements Cloneable, Comparable<Komputer>{
    public Laptop(String nazwa, LocalDate dataProdukcji, boolean czyApple){
        super(nazwa,dataProdukcji);
        this.czyApple=czyApple;
    }

    public int compareTo(Laptop T) {
        if(this.nazwa==T.nazwa)
            if(this.dataProdukcji==T.dataProdukcji)
                if(this.czyApple==T.czyApple)
                    return 1;
        return 0;
    }
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Laptop laptop = (Laptop) o;
        return nazwa.equals(laptop.nazwa) &&
                dataProdukcji.equals(laptop.dataProdukcji);
    }

    private String nazwa;
    private LocalDate dataProdukcji;
    private boolean czyApple;


}
