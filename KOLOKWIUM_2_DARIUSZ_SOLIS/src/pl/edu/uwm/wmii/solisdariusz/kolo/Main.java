package pl.edu.uwm.wmii.solisdariusz.kolo;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;

import pl.imiajd.solis.Komputer;
import pl.imiajd.solis.Laptop;
public class Main {

    public static void main(String[] args)
    {
        ArrayList<Komputer> grupa=new ArrayList<>();

        grupa.add(new Komputer("micro", LocalDate.parse("1999-01-29")));
        grupa.add(new Komputer("micro", LocalDate.parse("1998-02-27")));
        grupa.add(new Komputer("Gamingowy", LocalDate.parse("2007-12-19")));
        grupa.add(new Komputer("CyberPunkowy", LocalDate.parse("2077-12-10")));
        grupa.add(new Komputer("Moj", LocalDate.parse("2007-12-19")));

        for (Komputer p:
                grupa) {
            System.out.println(p.toString());
        }

        System.out.println();
        Collections.sort(grupa);
        System.out.println();

        for (Komputer p:
                grupa) {
            System.out.println(p.toString());
        }
        ArrayList<Laptop> grupaLaptopow=new ArrayList<>();

        grupaLaptopow.add(new Laptop("Apple", LocalDate.parse("1999-01-29"),true));
        grupaLaptopow.add(new Laptop("Apple", LocalDate.parse("1998-02-27"),true));
        grupaLaptopow.add(new Laptop("Gamingowy", LocalDate.parse("2007-12-19"),false));
        grupaLaptopow.add(new Laptop("CyberPunkowy", LocalDate.parse("2077-12-10"),false));
        grupaLaptopow.add(new Laptop("Moj", LocalDate.parse("2007-12-19"),false));

        for (Laptop x:
                grupaLaptopow) {
            System.out.println(x.toString());
        }

        System.out.println();
        Collections.sort(grupaLaptopow);
        System.out.println();

        for (Laptop x:
                grupaLaptopow) {
            System.out.println(x.toString());
        }
        //Zadanie2
        LinkedList<String> nowe =new LinkedList<String>();
        System.out.println("\n Zadanie2");
        for (Komputer p:
                grupa) {
            nowe.add(p.toString());
        }
        for (String p:
                nowe) {
            System.out.println(p);
        }
        Komputer.redukuj(nowe, 2);
        System.out.println("\n\nWynik zad2\n\n");
        for (String p:
                nowe) {
            System.out.println(p);
        }
        //koniec Zadanie2

    }
}

