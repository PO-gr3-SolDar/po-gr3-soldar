package pl.edu.uwm.wmii.solisdariusz.laboratorium03;

import java.util.Scanner;
import java.util.Random;
public class Main {

    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);
        System.out.println("Podaj liczbe n z przedzialu 1 <= n <= 100");
        int n = in.nextInt();
        Random r = new Random();
        int i, p = 0, np = 0;
        int[] tab = new int[n];
        if(n >= 100 || n <= 1)
        {
            System.out.println("Uruchom ponownie program i podaj liczbe z przedzialu 1 <= n <= 100");
            return;
        }
        for(i = 0 ; i < n ; i++)
        {
            tab[i] = r.nextInt(1999)-999;
            if(tab[i]%2 == 0)
                p++;
            else
                np++;
        }
        System.out.println("Parzystych " + np);
        System.out.println("Nieparzystych " + p);
    }
}
