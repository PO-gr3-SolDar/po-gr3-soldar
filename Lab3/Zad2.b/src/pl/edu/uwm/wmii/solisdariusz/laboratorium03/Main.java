package pl.edu.uwm.wmii.solisdariusz.laboratorium03;
import java.util.Random;

public class zad2_b {

    public static class Typ{
        int zer(int tab[], int n){
            int wynik=0;
            int p=0;
            for(int i=0; i<n; i++){
                if (tab[i]==0)
                    wynik++; //elementy zerowe
            }
            return wynik;
        }
        int dod(int tab[], int n){
            int wynik=0;
            int p=0;
            for(int i=0; i<n; i++){
                if (tab[i]>0)
                    wynik++; //elementy dodatnie
            }
            return wynik;
        }
        int uj(int tab[], int n){
            int wynik=0;
            int p=0;
            for(int i=0; i<n; i++){
                if (tab[i]<0)
                    wynik++; //elementy ujemne
            }
            return wynik;
        }
    }


    public static void main(String[] args) {
        int n=13;
        Random r=new Random();
        int tab[]=new int[n];
        for(int i=0; i<n; i++){
            tab[i]=r.nextInt(1999)-999;
            System.out.print(tab[i]+"\t");
        }
        System.out.println();
        Typ x = new Typ();
        System.out.println("Ilość elementów zerowych w tablicy: " + x.zer(tab, n));
        System.out.println("Ilość elementów dodatnich w tablicy: " + x.dod(tab, n));
        System.out.println("Ilość elementów ujemnych w tablicy: " + x.uj(tab, n));
    }
}
