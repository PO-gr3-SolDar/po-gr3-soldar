package pl.edu.uwm.wmii.solisdariusz.laboratorium03;

import java.util.Random;
import java.util.Scanner;

public class Main
{
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);
        System.out.println("Podaj liczbe n z przedzialu 1 <= n <= 100");
        int n = in.nextInt();
        System.out.println("Podaj liczbe Lewy spelniajaca 1 ≤ lewy < n,1 ≤ prawy < n");
        int lewy = in.nextInt();
        System.out.println("Podaj liczbe Prawy spelniajaca 1 ≤ lewy < n,1 ≤ prawy < n");
        int prawy = in.nextInt();
        Random r = new Random();
        if(n > 100 || n < 1 || lewy > 1 || lewy > n || 1 > prawy || prawy > n)
        {
            System.out.println("Uruchom ponownie program i podaj liczbe z przedzialu 1 <= n <= 100 lub Podaj liczbe Prawy, Lewy spelniajaca 1 ≤ lewy < n,1 ≤ prawy < n");
            return;
        }
        int[] tab = new int[n];
        int i, pojemnik, licznik = 1;
        for(i = 0 ; i < n ; i++)
        {
            tab[i] = r.nextInt(1999)-999;
        }
        for(i = lewy-1; i < (lewy + prawy)/2 ; i++)
        {
            pojemnik = tab[i];
            tab[i] = tab[prawy-licznik];
            tab[prawy-licznik] = pojemnik;
            licznik++;
        }
    }
}
