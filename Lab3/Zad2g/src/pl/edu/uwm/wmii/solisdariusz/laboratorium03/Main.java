package pl.edu.uwm.wmii.solisdariusz.laboratorium03;
import java.util.Random;

public class zad2_g {

    public static class Odwroc{
        void odwr(int tab[], int n, int l, int p){
            int pojemnik;
            int licznik=0;
            for(int i=l-1; i<(l+p)/2; i++){
                pojemnik=tab[i];
                tab[i]=tab[p-licznik];
                tab[p-licznik]=pojemnik;
                licznik++;
            }
            for(int i=0; i<n; i++){
                System.out.print(tab[i]+"   ");
            }
        }
    }

    public static void main(String[] args) {
        int n=13;
        Random r=new Random();
        int tab[]=new int[n];
        for(int i=0; i<n; i++){
            tab[i]=r.nextInt(1999)-999;
            System.out.print(tab[i]+"   ");
        }
        System.out.println();
        Odwroc x = new Odwroc();
        x.odwr(tab, n, 3, 9);
    }
}