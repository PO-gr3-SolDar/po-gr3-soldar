package pl.edu.uwm.wmii.solisdariusz.laboratorium03;
import java.util.Scanner;
import java.util.Random;
public class Main {

    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);
        System.out.println("Podaj liczbe n z przedzialu 1 <= n <= 100");
        int n = in.nextInt();
        Random r = new Random();
        if(n >= 100 || n <= 1)
        {
            System.out.println("Uruchom ponownie program i podaj liczbe z przedzialu 1 <= n <= 100");
            return;
        }
        int[] tab = new int[n];
        int d = 0, z = 0, u = 0, i;
        for(i = 0 ; i < n ; i++)
        {
            tab[i] = r.nextInt(1999)-999;
            if(tab[i] == 0)
                z++;
            else if(tab[i] > 0)
                d++;
            else
                u++;
        }
        System.out.println("Dodatnich: "+d);
        System.out.println("Ujemnych: "+u);
        System.out.println("Zerowych: "+z);

    }
}
