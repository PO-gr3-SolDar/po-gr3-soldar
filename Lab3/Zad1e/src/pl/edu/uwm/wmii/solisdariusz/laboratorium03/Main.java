package pl.edu.uwm.wmii.solisdariusz.laboratorium03;

import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Podaj liczbe n z przedzialu 1 <= n <= 100");
        int n = in.nextInt();
        Random r = new Random();
        if(n > 100 || n < 1)
        {
            System.out.println("Uruchom ponownie program i podaj liczbe z przedzialu 1 <= n <= 100");
            return;
        }
        int[] tab = new int[n];
        int i, licznik = 0, maks = 0;
        for(i = 0 ; i < n ; i++)
        {
            tab[i] = r.nextInt(1999)-999;
        }
        for(i = 0 ; i < n ; i++)
        {
            System.out.println(tab[i]);
            if(tab[i] >= 0)
                licznik++;
            else
            {
                if(maks < licznik)
                {
                    maks = licznik;
                    licznik = 0;
                }
                else
                    licznik = 0;
            }

        }

        System.out.println("najdłuższy fragment tablicy, w którym wszystkie elementy sa dodatnie wynosi: " + maks + " miejsc");
    }
}
