package pl.edu.uwm.wmii.solisdariusz.laboratorium03;
import java.util.Random;

public class zad2_f {

    public static class Zamien{
        void z(int tab[], int n){
            for(int i=0; i<n; i++){
                if (tab[i]>0) {
                    tab[i]=1;
                }
                else if(tab[i]<0)
                    tab[i]=-1;
            }
            for(int i=0; i<n; i++){
                System.out.print(tab[i]+"\t");
            }
        }
    }

    public static void main(String[] args) {
        int n=13;
        Random r=new Random();
        int tab[]=new int[n];
        for(int i=0; i<n; i++){
            tab[i]=r.nextInt(1999)-999;
            System.out.print(tab[i]+"\t");
        }
        System.out.println();
        Zamien x = new Zamien();
        x.z(tab, n);
    }
}
