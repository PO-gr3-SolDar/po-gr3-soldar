package pl.edu.uwm.wmii.solisdariusz.laboratorium03;

import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);
        System.out.println("Podaj liczbe n z przedzialu 1 <= n <= 100");
        int n = in.nextInt();
        Random r = new Random();
        if(n > 100 || n < 1)
        {
            System.out.println("Uruchom ponownie program i podaj liczbe z przedzialu 1 <= n <= 100");
            return;
        }
        int[] tab = new int[n];
        tab[0] = r.nextInt(1999)-999;
        int max = tab[0], i, ile = 0;
        for(i = 1 ; i < n ; i++)
        {
            tab[i] = r.nextInt(1999)-999;
            if(max < tab[i])
                max = tab[i];
        }
        for(i = 0 ; i < n ; i++)
        {
            if(tab[i] == max)
                ile++;
        }
        System.out.println("Max: " + max + "\n" + "Wystepuje: " + ile + " razy");
    }
}
