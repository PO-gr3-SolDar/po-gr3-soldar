package pl.edu.uwm.wmii.solisdariusz.laboratorium03;
import java.util.Random;

public class zad2_d {

    public static class Suma{
        int s_dod(int tab[], int n){
            int wynik=0;
            for(int i=0; i<n; i++)
                if (tab[i]>0)
                    wynik+=tab[i];
            return wynik;
        }
        int s_uj(int tab[], int n){
            int wynik=0;
            for(int i=0; i<n; i++)
                if (tab[i]<0)
                    wynik+=tab[i];
            return wynik;
        }
    }
    public static void main(String[] args) {
        int n=13;
        Random r=new Random();
        int tab[]=new int[n];
        for(int i=0; i<n; i++){
            tab[i]=r.nextInt(1999)-999;
            System.out.print(tab[i]+"\t");
        }
        System.out.println();
        Suma x = new Suma();
        System.out.println("Suma elementów dodatnich : " + x.s_dod(tab, n));
        System.out.println("Suma elementów ujemnych : " + x.s_uj(tab, n));
    }
}