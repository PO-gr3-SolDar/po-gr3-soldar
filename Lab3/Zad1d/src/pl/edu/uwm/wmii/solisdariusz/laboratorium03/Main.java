package pl.edu.uwm.wmii.solisdariusz.laboratorium03;

import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Podaj liczbe n z przedzialu 1 <= n <= 100");
        int n = in.nextInt();
        Random r = new Random();
        if(n > 100 || n < 1)
        {
            System.out.println("Uruchom ponownie program i podaj liczbe z przedzialu 1 <= n <= 100");
            return;
        }
        int[] tab = new int[n];
        int i, suma_uj = 0, suma_dod = 0;
        for(i = 0 ; i < n ; i++)
        {
            tab[i] = r.nextInt(1999)-999;
            if(tab[i] < 0)
                suma_uj += tab[i];
            else
                suma_dod += tab[i];
        }
        System.out.println("Suma ujemnych: " + suma_uj + "\n" + "Suma dodatnich " + suma_dod);
    }
}
