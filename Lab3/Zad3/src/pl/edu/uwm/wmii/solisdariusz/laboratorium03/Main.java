package pl.edu.uwm.wmii.solisdariusz.laboratorium03;
import java.util.Scanner;
import java.util.Random;
public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Podaj m z przedzialu [1;10]");
        int m = in.nextInt();
        System.out.println("Podaj n z przedzialu [1;10]");
        int n = in.nextInt();
        System.out.println("Podaj k z przedzialu [1;10]");
        int k = in.nextInt();

        if (m < 1 || m > 10 || n < 1 || n > 10 || k < 1 || k > 10) {
            System.out.println("Podane liczby nie sa z przedzialu [1;10], uruchom program ponownie i podaj prawidlowe dane");
            return;
        }

        Random r = new Random();
        int macierz1[][] = new int[m][n];
        int macierz2[][] = new int[n][k];
        int i, j;

        for (i = 0; i < m; i++) {
            for (j = 0; j < n; j++) {
                macierz1[i][j] = r.nextInt(10);
            }
        }

        for (i = 0; i < n; i++) {
            for (j = 0; j < k; j++) {
                macierz2[i][j] = r.nextInt(10);
            }
        }

        int wynik[][] = new int[m][k];
        for (i = 0; i < n; i++) {
            for (j = 0; j < k; j++) {
                wynik[i][j] = 0;
            }
        }

        System.out.println("Macierz A");

        for (i = 0; i < m; i++) {
            for (j = 0; j < n; j++) {
                System.out.print(macierz1[i][j] + " ");
            }
            System.out.println();
        }

        System.out.println();
        System.out.println("Macierz B");

        for (i = 0; i < n; i++) {
            for (j = 0; j < k; j++) {
                System.out.print(macierz2[i][j] + " ");
            }
            System.out.println();
        }

        int suma = 0;
        for (i = 0; i < m; i++) {
            for (j = 0; j < k; j++) {
                for(int z = 0 ; z < n ; z++)
                {
                    suma += macierz1[i][z] * macierz2[z][j];
                }
                wynik[i][j] = suma;
                suma = 0;
            }
        }

        System.out.println();
        System.out.println("Iloczyn macierzy A * B");

        for (i = 0; i < m; i++) {
            for (j = 0; j < k; j++) {
                System.out.print(wynik[i][j] + " ");
            }
            System.out.println();
        }
    }
}
