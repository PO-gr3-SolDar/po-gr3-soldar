package pl.edu.uwm.wmii.solisdariusz.laboratorium03;

public class zad2_a {
        public class Ile {
            int parz(int[] tab, int n) {
                int p = 0;
                for (int i = 0; i < n; i++) {
                    if (tab[i] % 2 == 0)
                        p++;
                }
                return p;
            }

            int nparz(int[] tab, int n) {
                int np = 0;
                for (int i = 0; i < n; i++) {
                    if (tab[i] % 2 != 0)
                        np++;
                }
                return np;
            }
        }
}
