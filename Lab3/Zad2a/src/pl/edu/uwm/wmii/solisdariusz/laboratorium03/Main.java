package pl.edu.uwm.wmii.solisdariusz.laboratorium03;

import java.util.Random;
import java.util.Scanner;

public class Main
{
    public class zad2_a {

            public static int ileParzystych(int[] tab) {
                int p = 0;
                int n = tab.length;
                for (int i = 0; i < n; i++) {
                    if (tab[i] % 2 == 0)
                        p++;
                }
                return p;
            }
            public static int ileNieparzystych(int[] tab) {
                int np = 0;
                int n = tab.length;
                for (int i = 0; i < n; i++) {
                    if (tab[i] % 2 != 0)
                        np++;
                }
                return np;
            }
    }
    public void main(String[] args)
    {
        Scanner in = new Scanner(System.in);
        System.out.println("Podaj liczbe n z przedzialu 1 <= n <= 100");
        int n = in.nextInt();
        Random r = new Random();
        if(n > 100 || n < 1)
        {
            System.out.println("Uruchom ponownie program i podaj liczbe z przedzialu 1 <= n <= 100");
            return;
        }
        int[] tab = new int[n];
        int i;
        for(i = 0 ; i < n ; i++)
        {
            tab[i] = r.nextInt(1999)-999;
        }
        System.out.println("nieparzystych" + zad2_a.ileNieparzystych(tab));
        System.out.println("parzystych" + zad2_a.ileParzystych(tab));
    }
}

