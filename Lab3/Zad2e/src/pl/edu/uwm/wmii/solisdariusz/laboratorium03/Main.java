package pl.edu.uwm.wmii.solisdariusz.laboratorium03;
import java.util.Random;

public class zad2_e {

    public static class Ciag{
        int naj_d(int tab[], int n){
            int licznik=0;
            int maks=licznik;
            for(int i=0; i<n; i++){
                if (tab[i]>0) {
                    licznik++;
                    if(licznik>maks)
                        maks=licznik;
                }
                else if(tab[i]<0)
                    licznik=0;
            }
            return maks;
        }
    }

    public static void main(String[] args) {
        int n=13;
        Random r=new Random();
        int tab[]=new int[n];
        for(int i=0; i<n; i++){
            tab[i]=r.nextInt(1999)-999;
            System.out.print(tab[i]+"\t");
        }
        System.out.println();
        Ciag x = new Ciag();
        System.out.println("Najdluzszy ciag elementow dodatnich : " + x.naj_d(tab, n));
    }
}