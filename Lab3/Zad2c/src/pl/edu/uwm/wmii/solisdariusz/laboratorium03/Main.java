package pl.edu.uwm.wmii.solisdariusz.laboratorium03;
import java.util.Random;

public class zad2_c {

    public static class Maks{
        int ile(int tab[], int n){
            int maks=tab[0];
            int wynik=0;
            int p=0;
            for(int i=0; i<n; i++)
                if (tab[i]>maks)
                    maks = tab[i];
            for(int i=0; i<n; i++)
                if (tab[i]==maks)
                    wynik++;
            return wynik;
        }
    }
    public static void main(String[] args) {
        int n=13;
        Random r=new Random();
        int tab[]=new int[n];
        for(int i=0; i<n; i++){
            tab[i]=r.nextInt(1999)-999;
            System.out.print(tab[i]+"\t");
        }
        System.out.println();
        Maks x = new Maks();
        System.out.println("Element maksymalny występuje : " + x.ile(tab, n) + " razy");
    }
}
