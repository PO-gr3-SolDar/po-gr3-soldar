package pl.edu.uwm.wmii.solisdariusz.laboratorium07;

import java.util.Collection;
import java.util.Collections;
import java.time.LocalDate;
import java.util.ArrayList;

import org.w3c.dom.html.HTMLOListElement;
import pl.imiajd.Solis.Osoba;

public class Main {

    public static void main(String[] args)
    {
	ArrayList<Osoba> spolecznosc = new ArrayList<Osoba>();
	spolecznosc.add(new Osoba("Solis", LocalDate.parse("1999-04-08")));
	spolecznosc.add(new Osoba("Szynaka", LocalDate.parse("1985-12-24")));
	spolecznosc.add(new Osoba("Mokebe", LocalDate.parse("2000-01-24")));
	spolecznosc.add(new Osoba("Drakula", LocalDate.parse("1980-05-28")));
	spolecznosc.add(new Osoba("Guccio-Gucci", LocalDate.parse("1921-06-28")));

    for(Osoba x : spolecznosc)
    {
        System.out.println(x.toString());
    }
    Collections.sort(spolecznosc);
    System.out.println("\n\n\n\n");
    for(Osoba x : spolecznosc)
    {
        System.out.println(x.toString());
    }

    }
}
