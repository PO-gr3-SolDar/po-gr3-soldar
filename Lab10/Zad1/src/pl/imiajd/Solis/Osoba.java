package pl.imiajd.Solis;
import java.time.LocalDate;
public class Osoba implements Cloneable, Comparable<Osoba>{


    private String nazwisko;
    private LocalDate dataUrodzenia;

    public Osoba(String nazwisko, LocalDate dataUrodzenia){
        this.dataUrodzenia=dataUrodzenia;
        this.nazwisko=nazwisko;
    }

    public String toString()
    {
        return getClass()+" ["+nazwisko+"] "+dataUrodzenia;
    }

    public boolean equals(Osoba T)
    {
        if(this==T)
            return true;
        else
            return false;
    }

    public int compareTo(Osoba T)
    {
        if(this.nazwisko==T.nazwisko && this.dataUrodzenia==T.dataUrodzenia)
            return 1;
        else
            return 0;
    }

}
