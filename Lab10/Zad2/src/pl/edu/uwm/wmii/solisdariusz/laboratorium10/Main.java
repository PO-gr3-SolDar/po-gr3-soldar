package pl.edu.uwm.wmii.solisdariusz.laboratorium10;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;

//Program TestStudent
public class Main {

    public static void main(String[] args)
    {
        ArrayList<Student> spolecznosc = new ArrayList<Student>();
        spolecznosc.add(new Student("Solis", LocalDate.parse("1999-04-08"),3.0));
        spolecznosc.add(new Student("Szynaka", LocalDate.parse("1985-12-24"), 5.0));
        spolecznosc.add(new Student("Mokebe", LocalDate.parse("2000-01-24"), 6.0));
        spolecznosc.add(new Student("Drakula", LocalDate.parse("1980-05-28"),4.0));
        spolecznosc.add(new Student("Guccio-Gucci", LocalDate.parse("1921-06-28"), 6.0));

        for(Student x : spolecznosc)
        {
            System.out.println(x.toString());
        }
        Collections.sort(spolecznosc);
        System.out.println("\n\n\n\n");
        for(Student x : spolecznosc)
        {
            System.out.println(x.toString());
        }

    }
}
