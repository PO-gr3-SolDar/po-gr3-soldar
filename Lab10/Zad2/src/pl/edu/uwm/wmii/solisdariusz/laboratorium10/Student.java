package pl.edu.uwm.wmii.solisdariusz.laboratorium10;

import java.time.LocalDate;

public class Student extends Osoba implements Comparable<Osoba>, Cloneable
{
    private double sredniaOcen;
    public Student(String nazwisko, LocalDate dataUrodzenia, double sredniaOcen)
    {
        super(nazwisko, dataUrodzenia);
        this.sredniaOcen=sredniaOcen;
    }

    public String toString()
    {
        return getClass()+" ["+nazwisko+"] "+dataUrodzenia+" srednia ocen: "+sredniaOcen;
    }

    public int compareTo(Student T)
    {
        if(this.nazwisko==T.nazwisko && this.dataUrodzenia == T.dataUrodzenia && this.sredniaOcen == T.sredniaOcen)
        {
            return 1;
        }
        else
            return 0;
    }
}