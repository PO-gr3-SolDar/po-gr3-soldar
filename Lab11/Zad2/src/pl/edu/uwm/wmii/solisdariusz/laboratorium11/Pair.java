package pl.edu.uwm.wmii.solisdariusz.laboratorium11;

public class Pair<T> {

    private T pomoc=null;
    private T first;
    private T second;

    public Pair() {
        first = null;
        second = null;
    }

    public Pair (T first, T second) {
        this.first = first;
        this.second = second;
    }

    public T getFirst() {
        return first;
    }
    public T getSecond() {
        return second;
    }

    public void setFirst (T newValue) {
        first = newValue;
    }
    public void setSecond (T newValue) {
        second = newValue;
    }

    public <T> void swap(){
        pomoc= this.first;
        this.first=this.second;
        this.second=pomoc;
    }

}