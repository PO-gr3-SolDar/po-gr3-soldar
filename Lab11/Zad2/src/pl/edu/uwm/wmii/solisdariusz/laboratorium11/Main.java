package pl.edu.uwm.wmii.solisdariusz.laboratorium11;

public class Main {

    public static void main(String[] args) {
        String[] slowa = {"Ala","ma","kota","a","kot","ma","Ale"};
        Pair<String> minmax = ArrayAlg.minmax(slowa);
        System.out.println("min: " + minmax.getFirst());
        System.out.println("max: " + minmax.getSecond());
        Pair<String> x = PairUtil.swap(minmax);
        System.out.println("min: " + x.getFirst());
        System.out.println("max: " + x.getSecond());

    }
}

class ArrayAlg
{
    public static Pair<String> minmax(String[] a)
    {
        if(a == null)
            return null;
        String min = a[0], max = a[0];
        for(int i = 1; i < a.length ; i++)
        {
            if(min.length() > a[i].length())
                min = a[i];
            if(max.length() < a[i].length())
                max = a[i];
        }
        return new Pair<String> (min,max);
    }
}
