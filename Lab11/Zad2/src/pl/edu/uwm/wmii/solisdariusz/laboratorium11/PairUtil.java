package pl.edu.uwm.wmii.solisdariusz.laboratorium11;

public class PairUtil<T>
{

    public static <T> Pair<T> swap(Pair<T> x){
        Pair<T> pomoc = new Pair<T>();
        x.setFirst(x.getSecond());
        x.setSecond(x.getFirst());
        return x;
    }
}