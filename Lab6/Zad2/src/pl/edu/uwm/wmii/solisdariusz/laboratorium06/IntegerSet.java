package pl.edu.uwm.wmii.solisdariusz.laboratorium06;

public class IntegerSet
{
    Boolean[] elementy = new Boolean[100];

    public IntegerSet(int tab[])
    {
        int i = 0;
        while(i<tab.length) { elementy[tab[i]] = true; i++;}
        i = 0;
        while(i<100) { if(elementy[i] == null) elementy[i] = false;}
    }

    public void getElements()
    {
        int i = 0;
        while(i<100) { if(elementy[i]) System.out.println(i+"\n");}
    }

    public static Boolean[] union(Boolean tab1[], Boolean tab2[])
    {
        int i = 0;
        Boolean[] pomoc = new Boolean[100];
        while(i<100)
        {
            if(tab1[i] || tab2[i]) pomoc[i] = true;
            else pomoc[i] = false;
            i++;
        }
        return pomoc;
    }

    public static Boolean[] intersection(Boolean tab1[], Boolean tab2[])
    {
        Boolean[] pomoc = new Boolean[100];
        int i = 0;
        while(i<100)
        {
            if(tab1[i] && tab2[i]) pomoc[i] = true;
            else pomoc[i] = false;
            i++;
        }
        return pomoc;
    }

    public void insertElement(int a)
    {
        this.elementy[a]=true;
    }

    public void deleteElement(int a)
    {
        this.elementy[a]=false;
    }

    public String toString()
    {
        String pomoc= "";
        for(int i = 0 ; i < 100 ; i++) if(elementy[i]) pomoc+=(i+" ");
        return pomoc;
    }

    public void equals(Boolean set2[])
    {
        Boolean wynik = true;
        for(int i = 0 ; i < 100 ; i++)
        {
            if(elementy[i] != set2[i])
            {
                wynik = false;
                break;
            }
        }
        if(wynik) System.out.println("Zbiory sa rowne");
        else System.out.println("Zbiory nie sa rowne");
    }
}
