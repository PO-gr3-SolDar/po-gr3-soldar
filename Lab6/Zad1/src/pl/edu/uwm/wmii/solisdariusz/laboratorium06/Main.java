package pl.edu.uwm.wmii.solisdariusz.laboratorium06;

public class Main
{

    public static void main(String[] args)
    {
            RachunekBankowy saver1 = new RachunekBankowy(2000);
            RachunekBankowy saver2 = new RachunekBankowy(3000);
            saver1.setRocznaStopaProcentowa(0.04);
            saver2.setRocznaStopaProcentowa(0.04);
            saver1.obliczMiesieczneOdsetki();
            saver1.getSaldo();
            saver2.obliczMiesieczneOdsetki();
            saver2.getSaldo();
            saver1.setRocznaStopaProcentowa(0.05);
            saver2.setRocznaStopaProcentowa(0.05);
            saver1.obliczMiesieczneOdsetki();
            saver1.getSaldo();
            saver2.obliczMiesieczneOdsetki();
            saver2.getSaldo();
    }
}
