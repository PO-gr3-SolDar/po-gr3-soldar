package pl.edu.uwm.wmii.solisdariusz.laboratorium06;

public class RachunekBankowy
{
    static double rocznaStopaProcentowa;
    private double saldo;

    public RachunekBankowy(double saldo)
    {
        this.saldo = saldo;
    }

    public void obliczMiesieczneOdsetki()
    {
        double odsetki = (saldo * rocznaStopaProcentowa)/12;
        this.saldo += odsetki;
    }

    public void setRocznaStopaProcentowa(double a)
    {
        this.rocznaStopaProcentowa=a;
    }

    public void getSaldo()
    {
        System.out.format("%.2f%n", saldo);
        System.out.println();
    }
}
