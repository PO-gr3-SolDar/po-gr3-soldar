package pl.edu.uwm.wmii.solisdariusz.laboratorium00;

import java.text.DecimalFormat;

public class Main {
//zalozenie kapitalizacja co miesiac
    public static void main(String[] args)
    {
	    double saldo = 1000.00;
	    DecimalFormat df = new DecimalFormat("###.##");
	    for(int i = 1 ; i < 37 ; i++)
        {
            saldo = 1.005 * saldo;
            if(i%12==0)
            {
                System.out.println("saldo po " + i/12 + " roku wynosi: " + df.format(saldo) + " zl");
            }
        }

    }

}
