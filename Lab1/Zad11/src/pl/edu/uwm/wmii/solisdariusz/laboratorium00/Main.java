package pl.edu.uwm.wmii.solisdariusz.laboratorium00;
//zrodlo wiersza: https://www.facebook.com/studenttypowy/posts/976989509059406/
public class Main {

    public static void main(String[] args) {
    String wiersz = "Fikuśny wiersz o studiach.\n" +
            "Z rana wykłady, potem ćwiczenia,\n" +
            "I każdy z nas ma wielkiego lenia,\n" +
            "Notatki na kolosy wszyscy zbieramy,\n" +
            "A i tak na poprawy często uczęszczamy,\n" +
            "W końcu kolokwia udaje się zdać,\n" +
            "I na egzamin pora już gnać,\n" +
            "Stresu pełno co niemiara,\n" +
            "Jak obleję będzie siara,\n" +
            "Lecz tu z nieba jest ratunek,\n" +
            "Gdyż możemy wziąć warunek.\n" +
            "Puenta wiersza jest the best,\n" +
            "Gdyż warunek drogi jest.\n" +
            "Bądź cierpliwy i rozważny,\n" +
            "By zdać egzamin każdy.\n" +
            "Nagórski";
    System.out.println(wiersz);
    }
}
