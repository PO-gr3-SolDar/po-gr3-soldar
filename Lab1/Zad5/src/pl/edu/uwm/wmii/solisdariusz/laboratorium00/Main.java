package pl.edu.uwm.wmii.solisdariusz.laboratorium00;

public class Main
{
    public static void main(String[] args)
    {
        String slowo = "Java";
        String obramowanie_gorne = new String();
        String obramowanie_dolne = new String();
        int ilosc_liter_slowa = slowo.length();
        String gorna = "-";
        String srodek = "|"+slowo+"|";
        String dolna = "-";
        for(int i = 0 ; i <= ilosc_liter_slowa+1 ; i++)
        {
            if(i == 0 || i == ilosc_liter_slowa+1)
            {
                obramowanie_gorne += "+";
                obramowanie_dolne += "+";
            }
            else
            {
                obramowanie_gorne += gorna;
                obramowanie_dolne += dolna;
            }
        }
        System.out.println(obramowanie_gorne+"\n"+srodek+"\n"+obramowanie_dolne);
    }
}
