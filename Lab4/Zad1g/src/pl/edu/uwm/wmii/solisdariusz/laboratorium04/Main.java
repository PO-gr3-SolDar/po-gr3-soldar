package pl.edu.uwm.wmii.solisdariusz.laboratorium04;

import java.util.Scanner;

public class Main {
    public static String nice(String a){
        StringBuffer wynik=new StringBuffer();
        int l=a.length();
        int licznik=1;
        for(int i=0; i<l; i++){
            if (licznik==3) {
                wynik.append(a.charAt(i));
                wynik.append("'");
                licznik=0;
            }
            else{
                wynik.append(a.charAt(i));
            }
            licznik++;
        }
        return wynik.toString();
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Podaj napis do zmodyfikowania: ");
        String napis=in.nextLine();
        System.out.println("Napis po modyfikacji: "+nice(napis));
    }
}