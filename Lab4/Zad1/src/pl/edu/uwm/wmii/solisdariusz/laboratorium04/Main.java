package pl.edu.uwm.wmii.solisdariusz.laboratorium04;

import java.util.Scanner;

public class Main {

    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);
        System.out.println("Wprowadz napis: ");
        String napis = in.nextLine();
        System.out.println("Podaj znak którego ilość mam policzyć: ");
        char c = in.next().charAt(0);
        System.out.println("Podany znak występuje: "+ zad1a.ilosc(napis, c) + " razy");

    }

}

public class zad1a
{
    public static int ilosc(String a, char b)
    {
        int dlugosc = a.length(), licznik = 0, i;
        for(i = 0 ; i < dlugosc ; i++)
        {
            if(a.charAt(i)==b)
                licznik++;
        }
        return licznik;
    }
}