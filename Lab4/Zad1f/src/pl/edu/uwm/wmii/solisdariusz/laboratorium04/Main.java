package pl.edu.uwm.wmii.solisdariusz.laboratorium04;
import java.util.Scanner;
public class Main {
    public static String change(String a) {
        StringBuffer buf = new StringBuffer(a);
        int l = a.length();
        char placeholder = ' ';
        for (int i = 0; i < l; i++) {
            if (a.charAt(i) == a.toUpperCase().charAt(i)) {
                placeholder = a.toLowerCase().charAt(i);
                buf.setCharAt(i, placeholder);
            } else {
                placeholder = a.toUpperCase().charAt(i);
                buf.setCharAt(i, placeholder);
            }
        }
        return buf.toString();
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Podaj napis do zmodyfukowania: ");
        String napis=in.nextLine();
        System.out.println("Napis po modyfikacji: " + change(napis));
    }
}
