package pl.edu.uwm.wmii.solisdariusz.laboratorium04;
import java.util.Scanner;

public class Main {
    public static String nice(String a, char s, int n){
        StringBuffer wynik=new StringBuffer();
        int l=a.length();
        int licznik=1;
        for(int i=0; i<l; i++){
            if (licznik==n) {
                wynik.append(a.charAt(i));
                wynik.append(s);
                licznik=0;
            }
            else{
                wynik.append(a.charAt(i));
            }
            licznik++;
        }
        return wynik.toString();
    }


    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.println("Podaj napis do modyfikacji: ");
        String napis = in.nextLine();

        System.out.println("Podaj separator: ");
        String x = in.nextLine();

        System.out.println("Podaj skok: ");
        int n=in.nextInt();

        StringBuffer s = new StringBuffer(x);
        char sep = s.charAt(0);

        System.out.println("Napis po modyfikacji: " + nice(napis, sep, n));
    }
}