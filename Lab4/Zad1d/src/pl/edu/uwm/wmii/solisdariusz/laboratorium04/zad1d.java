package pl.edu.uwm.wmii.solisdariusz.laboratorium04;

import java.util.Scanner;

public class zad1d {

    public static String repeat(String a, int n)
    {
        String wynik="";
        for(int i=0; i<n; i++)
            wynik+=a;
        return wynik;
    }

    public static void main(String[] args)
    {
        Scanner input=new Scanner(System.in);
        System.out.println("Podaj napis: ");
        String napis=input.nextLine();
        System.out.println("Podaj ilość konkatenacji: ");
        int n=input.nextInt();
        System.out.println("Otrzymany napis to "+repeat(napis, n));
    }
}
