package pl.imiajd.Solis;

import java.time.LocalDate;

public abstract class Instrument
{
    public Instrument(String producent, LocalDate rokProdukcji)
    {
        this.producent = producent;
        this.rokProdukcji = rokProdukcji;
    }
    public String getProducent()
    {
        return producent;
    }
    public LocalDate getRokProdukcji()
    {
        return rokProdukcji;
    }
    public String toString()
    {
        return "Producentem jest: "+producent+" wykoanany w: "+rokProdukcji+" roku";
    }
    public boolean equals(Instrument x)
    {
        if(x.getProducent()==producent)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public abstract String dzwiek();
    private String producent;
    private LocalDate rokProdukcji;
}

