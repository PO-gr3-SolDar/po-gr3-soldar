package pl.edu.uwm.wmii.solisdariusz.laboratorium05;
import pl.imiajd.Solis.*;
import java.time.LocalDate;
import java.util.ArrayList;


public class Main {

    public static void main(String[] args) {
	    ArrayList<Instrument> Orkiestra = new ArrayList<Instrument>();

	    Orkiestra.add(new Flet("Yamaha", LocalDate.now()));
        Orkiestra.add(new Fortepian("Yamaha", LocalDate.now()));
        Orkiestra.add(new Skrzypce("Yamaha", LocalDate.now()));

        for(Instrument p: Orkiestra)
        {
            System.out.println(p.dzwiek());
        }

        System.out.println("\n\n");

        for(Instrument p: Orkiestra)
        {
            System.out.println(p.toString());
        }
    }
}