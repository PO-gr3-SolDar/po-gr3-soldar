package pl.edu.uwm.wmii.solisdariusz.laboratorium05;
import java.time.LocalDate;
import java.util.*;
import pl.imiajd.Solis.*;
public class Main
{
    public static void main(String[] args)
    {
        String[] ja = new String[1];
        ja[0] = "Dariusz";
        Osoba[] ludzie = new Osoba[2];

        ludzie[0] = new Pracownik(ja, "Solis", LocalDate.now(),true,0, LocalDate.now());
        ludzie[1] = new Student(ja, "Solis", LocalDate.now(),true,"informatyka", 3.0);
        // ludzie[2] = new Osoba("Dyl Sowizdrzał");

        for (Osoba p : ludzie) {
            System.out.println(p.getNazwisko() + ": " + p.getOpis());
        }
    }
}


