package pl.imiajd.Solis;

import java.time.LocalDate;

public class Student extends Osoba
{
    public Student(String[] imiona, String nazwisko, LocalDate dataUrodzenia,boolean plec, String kierunek, double sredniaOcen)
    {
        super(imiona, nazwisko, dataUrodzenia, plec);
        this.kierunek = kierunek;
        this.sredniaOcen = sredniaOcen;
    }

    public String getOpis()
    {
        return "kierunek studiów: " + kierunek;
    }
    public double getSredniaOcen()
    {
        return sredniaOcen;
    }
    public void setSredniaOcen(double sredniaOcen1)
    {
        this.sredniaOcen = sredniaOcen1;
    }


    private String kierunek;
    private double sredniaOcen;
}