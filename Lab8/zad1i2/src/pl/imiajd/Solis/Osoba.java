package pl.imiajd.Solis;

import java.time.LocalDate;

public abstract class Osoba
{
    public Osoba(String[] imiona, String nazwisko, LocalDate dataUrodzenia, boolean plec)
    {
        this.imiona = imiona;
        this.nazwisko = nazwisko;
        this.dataUrodzenia = dataUrodzenia;
        this.plec = plec;
    }
    public String[] getImiona()
    {
        return imiona;
    }

    public String getNazwisko()
    {
        return nazwisko;
    }

    public LocalDate getDataUrodzenia()
    {
        return dataUrodzenia;
    }

    public String getPlec()
    {
        if(plec == true)
            return "mezczyzna";
        else
            return "kobieta";
    }

    public abstract String getOpis();

    private String[] imiona;
    private String nazwisko;
    private LocalDate dataUrodzenia;
    private boolean plec;
}
